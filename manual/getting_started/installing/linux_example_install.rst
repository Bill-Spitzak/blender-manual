
*******************
Linux example install of downloaded Blender
*******************

The following commands sucessfully installed the downloaded 4.0.2 version of Blender
on POP!_OS (a spinoff of Ubuntu). This includes making the ``.blend`` files show
thumbnails and icons and making double-clicking of them launch Blender.

This should work for other Linux distributions, however the directories files must be
placed into, especially icons, can change depending on the distribution and the
desktop theme. Search around for similar directory names and files to locate where
they should go.  `Freedesktop.org <https://www.freedesktop.org/wiki/Specifications/>`__
is the official specification for how these files work.

Use of ``sudo`` can be avoided by unpacking the download into your home directory, using
that instead of ``/opt``, and making all the links to ``~/.local`` instead of ``/usr/local``.

**Extract the downloaded ``.tar.xz`` file into a directory in ``/opt``**

   .. code-block:: sh

      sudo mkdir -p /opt
      cd /opt
      sudo tar -xf ~/Downloads/blender-4.0.2-linux-x64.tar.xz

**Add a symbolic link to skip the version number**

   .. code-block:: sh

      sudo ln -sTf blender-4.0.2-linux-x64 /opt/blender

**Make Blender commands work from the command line**

   .. code-block:: sh

      sudo ln -s /opt/blender/blender /usr/bin/blender
      sudo ln -s /opt/blender/blender-thumbnailer /usr/bin/blender-thumbnailer

   You should now be able to type ``blender`` in a shell to run it.

**Make the desktop know about the blender application**

   .. code-block:: sh

      sudo ln -s /opt/blender/blender.desktop /usr/share/applications/

   It should now appear in your Application selector, but the icon is missing.

**Make the icon for the blender application work**

   .. code-block:: sh

      sudo ln -s /opt/blender/blender.svg /usr/share/icons/hicolor/scalable/apps/blender.svg

   Here you need to locate the correct directory for your Linux distribution.

**Check if the "mime type" exists**

   This should work on any modern Linux installation.

   .. code-block:: console

      $ mimetype foo.blend
      foo.blend: application/x-blender

   Alternative:

   .. code-block:: console

      $ grep blender /etc/mime.types
      application/x-blender				blend

**Add the mimetype if missing**

   Put the following into ``/usr/share/mime/packages/Overrides.xml``:

   .. code-block:: xml

      <?xml version="1.0" encoding="UTF-8"?>
      <mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
        <mime-type type="application/x-blender">
          <comment>Blender scene</comment>
          <glob pattern="*.blend"/>
          <sub-class-of type="text/plain"/>
        </mime-type>
      </mime-info>

   And run this:

   .. code-block:: sh

      sudo update-mime-database /usr/share/mime

**Make the icon for .blend files work**

   .. code-block:: sh

      sudo ln -s /opt/blender/blender-symbolic.svg \
          /usr/share/icons/Pop/scalable/mimetypes/application-x-blender.svg
      sudo update-icon-caches /usr/share/icons/Pop/

   At least for this system, an entirely different subdirectory had to be used than
   the one for the execuatble icon.

**Make thumbnails work for .blend files**

   Thumbnails may already be working, try saving a ``.blend`` file and then looking
   at it in the files application. If not this should make them work:

   Put the following into ``/usr/share/thumbnailers/x-blender.thumbnailer``:

   .. code-block:: ini

      [Thumbnailer Entry]
      TryExec=/usr/bin/blender-thumbnailer
      Exec=/usr/bin/blender-thumbnailer %i %o
      MimeType=application/x-blender;
