.. index:: Geometry Nodes; Active Camera
.. _bpy.types.GeometryNodeInputActiveCamera:

******************
Active Camera Node
******************

.. figure:: /images/node-types_GeometryNodeInputActiveCamera.webp
   :align: right
   :alt: Active Camera node.

The *Active Camera* node outputs the scene's current active camera.


Inputs
======

This node has no inputs.


Properties
==========

This node has no properties.


Outputs
=======

Active Camera
   Current active camera.
