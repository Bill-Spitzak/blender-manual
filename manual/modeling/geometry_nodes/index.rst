.. index:: Nodes; Geometry Nodes
.. index:: Geometry Nodes
.. _bpy.types.GeometryNode:

##################
  Geometry Nodes
##################

.. toctree::
   :maxdepth: 3
   :titlesonly:

   introduction.rst
   inspection.rst
   attributes_reference.rst
   fields.rst
   instances.rst
   tools.rst


Node Types
==========

.. toctree::
   :maxdepth: 3
   :titlesonly:

   attribute/index.rst
   input/index.rst
   output/index.rst

-----

.. toctree::
   :maxdepth: 3
   :titlesonly:

   geometry/index.rst

-----

.. toctree::
   :maxdepth: 3
   :titlesonly:

   curve/index.rst
   instances/index.rst
   mesh/index.rst
   point/index.rst
   volume/index.rst

-----

.. toctree::
   :maxdepth: 3
   :titlesonly:

   simulation/simulation_zone.rst

-----

.. toctree::
   :maxdepth: 3
   :titlesonly:

   material/index.rst
   texture/index.rst
   utilities/index.rst

----

.. toctree::
   :maxdepth: 3
   :titlesonly:

   group.rst

----

.. toctree::
   :maxdepth: 3
   :titlesonly:

   hair/index.rst
   normals/index.rst


.. tip::

   :doc:`Asset Catalogs </files/asset_libraries/catalogs>`
   that contain geometry node groups will also appear in the add menu.
