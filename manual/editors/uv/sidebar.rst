
*******
Sidebar
*******

Image Tab
=========

UV Vertex
---------

The averaged-out position of the selected UV vertices.


Image
-----

See :doc:`/editors/image/image_settings`.


UDIM Tiles
----------

See :doc:`UDIM Tiles </modeling/meshes/uv/workflows/udims>`.


Tool Tab
========

Shows the settings for the active tool.


View Tab
========

Display
-------

You can set the editor's display options in this panel.

Aspect Ratio X, Y
   Display aspect for this image. Does not affect rendering.

Repeat Image
   Tile the image so it completely fills the editor.

.. _bpy.types.SpaceUVEditor.show_pixel_coords:

Pixel Coordinates
   Use pixel coordinates rather than relative coordinates (0 to 1) for the UV Vertex
   and 2D Cursor Location fields.


2D Cursor
---------

Location X, Y
   View and change the location of the 2D Cursor.


Annotations
-----------

Options for the :doc:`Annotate tool </interface/annotate_tool>`.


Scopes
======

See :ref:`editors-image-scopes` in the Image Editor.
