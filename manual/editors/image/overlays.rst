
********
Overlays
********

The Overlays pop-over configures the overlays that are displayed on top of images.
In the header, there is a button to turn off all overlays for the Image Editor.
This option also toggles the visibility of :doc:`UDIM </modeling/meshes/uv/workflows/udims>`
tile information.

The options that are visible in the pop-over depend on the Image Editor mode.
The following overlay categories are available:

Geometry
========

Display Texture Paint UVs
   Display the active object's UVs. The Image Editor must be in Paint mode, and the active object
   must be in Texture Paint Mode or Edit Mode for the UVs to be visible.


Image
=====

Show Metadata
   Displays metadata about the selected Render Result. See the Output tab's
   :doc:`/render/output/properties/metadata` panel to change what metadata to include.
